parent(bedros,filipp).
parent(boris,alla).
parent(edmuntas,kristina).
parent(alla,kristina).
parent(kristina,denis).
parent(alla,test).

spouse(filip,alla).
spouse(vladimir,kristina).

grandparent(X,Y) :- parent(X,Z), parent(Z,Y).
sibling(X,Y) :- parent(Z,X),parent(Z,Y),X\=Y.
uncle(X,Y) :- parent(Z,Y),sibling(X,Z).
