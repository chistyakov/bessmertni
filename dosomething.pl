dosomething([]).
dosomething([H|T]) :- member(H,T), dosomething(T).
dosomething([H|T]) :- write(H), dosomething(T).
