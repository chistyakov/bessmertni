fastsort([],[]).
fastsort([Head | Tail], ListSorted) :-
    split(Head, Tail, TailLess, TailGreater),
    fastsort(TailLess,TailLessSorted),
    fastsort(TailGreater,TailGreaterSorted),
    append(TailLessSorted, [Head | TailGreaterSorted], ListSorted).
split(_, [], [], []).
split(X, [H | T], [H | TL], TG) :- H < X, !, split(X, T, TL, TG).
split(X, [H | T], TL, [H | TG]) :- split(X, T, TL, TG).
