t("Skills").
onto("onto_skills.pro").
onto("onto_people.pro").
onto("common_eng.pro").


f("Python_prog_lang", "is_a", "skill").
f("Django_framework", "is_a", "skill").
f("Django_framework", "covers_skill", "Python_prog_lang").

f("HTML", "is_a", "skill").
f("CSS", "is_a", "skill").
f("Bootstrap", "is_a", "skill").
f("Bootstrap", "covers_skill", "HTML").
f("Bootstrap", "covers_skill", "CSS").

f("SQL", "is_a", "skill").
f("PostrgreeSQL", "is_a", "skill").
f("PostrgreeSQL", "covers_skill", "SQL").
f("MySQL", "is_a", "skill").
f("MySQL", "covers_skill", "SQL").
f("NoSQL", "is_a", "skill").
f("MongoDB", "is_a", "skill").
f("MongoDB", "covers_skill", "NoSQL").
f("Redis", "is_a", "skill").
f("Redis", "covers_skill", "NoSQL").

f("JavaScript", "is_a", "skill").
f("Nodejs", "is_a", "skill").
f("Nodejs", "covers_skill", "JavaScript").

f("git", "is_a", "skill").

f("Java", "is_a", "skill").

f("TDD", "is_a", "skill").


f("Alexander", "is_a", "candidacy").
f("Alexander", "has_surname", "Chistyakov").
f("Alexander", "has_a_skill", "Python_prog_lang").
f("Alexander", "has_a_skill", "git").
f("Alexander", "has_a_skill", "HTML").
f("Alexander", "has_a_skill", "CSS").

f("Irina", "is_a", "candidacy").
f("Irina", "has_surname", "Vorontsova").
f("Irina", "has_a_skill", "MySQL").
f("Irina", "has_a_skill", "Java").
f("Irina", "has_a_skill", "git").
f("Irina", "has_a_skill", "Bootstrap").
f("Irina", "has_a_skill", "TDD").


f("appscotch_Web_Developer_Django_Bootstrap", "is_a", "vacancy").
f("appscotch_Web_Developer_Django_Bootstrap", "require_skill", "Django_framework").
f("appscotch_Web_Developer_Django_Bootstrap", "require_skill", "Bootstrap").
f("appscotch_Web_Developer_Django_Bootstrap", "require_skill", "PostrgreeSQL").
f("appscotch_Web_Developer_Django_Bootstrap", "require_skill", "git").

f("fsecure_senior_se", "is_a", "vacancy").
f("fsecure_senior_se", "require_skill", "Java").
f("fsecure_senior_se", "require_skill", "MySQL").
f("fsecure_senior_se", "require_skill", "git").
f("fsecure_senior_se", "require_skill", "TDD").
f("fsecure_senior_se", "require_skill", "HTML").
