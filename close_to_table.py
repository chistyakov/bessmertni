INPUT_FILENAME = "URSI_080801_090201.dat"
OUTPUT_FILENAME = "closes2.dat"
DELIMETER = ','
NUMBER_OF_PREVIOUS_ELEMENTS = 5


def read_list_from_file(filename):
    ret_list = []
    with open(filename, 'r') as f:
        for line in f:
            append_close(ret_list, line)
    return ret_list

def append_close(list_to_append, str_to_parse):
    try:
        close = extract_close_value_from_string(str_to_parse)
        list_to_append.append(close)
    except ValueError:
        print "failed to extract close from string {0}".format(str_to_parse)

def extract_close_value_from_string(s):
    (ticker, per, datetime, close) = s.split(DELIMETER)
    return float(close)


def convert_list_to_table(list_of_closes):
    result_table = []
    for i in range(0, len(list_of_closes) - NUMBER_OF_PREVIOUS_ELEMENTS):
        previous_elements = list_of_closes[i:i+NUMBER_OF_PREVIOUS_ELEMENTS]
        next_element = list_of_closes[i+NUMBER_OF_PREVIOUS_ELEMENTS]
        next_diff = next_element - previous_elements[-1]
        row = previous_elements + [next_element] + [next_diff]
        result_table.append(row)
    return result_table


def write_table(table, filename):
    with open(filename, 'w') as f:
        if table:
            f.write(form_table_header())
            f.write("\r\n")
        for row in table:
            f.write(DELIMETER.join(str(close) for close in row))
            f.write("\r\n")

def form_table_header():
    return "Ci-4,Ci-3,Ci-2,Ci-1,Ci,Ci+1,Ci+1diff"

if __name__ == "__main__":
    list_of_closes = read_list_from_file(INPUT_FILENAME)
    print(len(list_of_closes))
    table = convert_list_to_table(list_of_closes)
    write_table(table, OUTPUT_FILENAME)
