c("vacancy").
c("skill").
c("general_skill").
c("candidacy").
o("candidacy", "AKO", "person").

pr(["covers_skill", "covered_by"]).
o("skill", "covers_skill", "skill").
r([t("?s1","covers_skill","?s2")],[t("?s2","covered_by","?s1")]).

pr(["has_a_skill", "is_competence_of_candidacy"]).
o("candidacy", "has_a_skill", "skill").
r([t("?c","has_a_skill","?s")],[t("?s","is_competence_of_candidacy","?c")]).
r([t("?c","has_a_skill","?s1"),t("?s1","covers_skill","?s2")],[t("?c","has_a_skill","?s2")]).

pr(["require_skill", "required"]).
o("vacancy", "require_skill", "skill").
r([t("?v","require_skill","?s")],[t("?s","required","?v")]).

pr(["does_not_fit", "fits"]).
r([t("?c","is_a","candidacy"),t("?v","is_a","vacancy"),t("?v","require_skill","?s"),n("?c","has_a_skill","?s")],[t("?c","does_not_fit","?v")]).
r([t("?c","is_a","candidacy"),t("?v","is_a","vacancy"),n("?c","does_not_fit","?v")],[t("?c","fits","?v")]).
