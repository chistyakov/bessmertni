dot_filename=$(basename "$1")
tex_filename="${dot_filename%.*}".tex
dot2tex -ftikz --prog=dot --figonly $dot_filename >$tex_filename
