lines = []
previous_value = 1.179

def extract_val(s):
    return float(s.split(',')[-1])

with open('URSI_080201_080801.dat', 'r') as f:
    for line in f:
        try:
            current_value = extract_val(line)
            diff = current_value - previous_value
            lines.append("{0},{1}".format(line.strip(), diff))
            previous_value = current_value
        except ValueError:
            lines.append("{0},{1}".format(line.strip(), 'DIFF'))


with open('out.dat', 'w') as f:
    for line in lines:
        f.write(line + "\n")
