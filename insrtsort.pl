insrtsort([],[]).
insrtsort([Head|Tail], ListSorted) :- insrtsort(Tail,TailSorted),
    insrt(Head,TailSorted,ListSorted).
insrt(X, [Y | ListSorted], [Y | ListSorted1]) :-
    X > Y, !, insrt(X, ListSorted, ListSorted1).
insrt(X,ListSorted, [X | ListSorted]).
